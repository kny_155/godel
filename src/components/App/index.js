import React, { Component } from 'react';
import Field from "../Field";
import Floor from "../Floor";
import Wall from "../Wall";

class App extends Component {

    movePerson = (x, y) => {
        const { xPerson, yPerson } = this.state;
        if(xPerson === x || yPerson === y){
          if(Math.abs(xPerson - x) === 1){
            const direction = xPerson - x < 0 ? 0 : 180;
            this.setState({
              xPerson: x,
              direction
            });
            return;
          }
          if(Math.abs(yPerson - y) === 1){
              const direction = yPerson - y < 0 ? 90 : 270;
              this.setState({
                  yPerson: y,
                  direction
              });
          }

        }
    };

    createField = () => {
        const field = [];

        for(let i = 0; i < 10; i++){
            const row = [];
            for(let j = 0; j < 6; j++){
                if(i === 0 || i === 9 || j === 0 || j === 5){
                    row[j] = <Wall  key={j + "_" + i}/>;
                } else {
                    row[j] = <Floor movePerson={() => this.movePerson(j, i)} key={j + "_" + i}/>;
                }

            }
            field.push(row);
        }
        return field;
    };


  state = {
      xPerson: 1,
      yPerson: 1,
      field: this.createField(),
      direction: 90,
      idTimer: null
  };

  render() {
    const {xPerson, yPerson, field, direction} = this.state;


    return (
      <div className="App" >
        <Field xPerson={xPerson} yPerson={yPerson} field={field} direction={direction}>
        </Field>
      </div>
    );
  }
}



export default App;
