import React from 'react';
import './style.css';

const Person = ({direction}) => {
    return (
        <div className='person' style={{transform: `rotate(${direction}deg)`}}/>
    )
};

export default Person;