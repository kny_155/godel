import React from 'react';
import './style.css';

const Floor = ({children, movePerson}) => {
    return (
        <div className='floor' onClick={movePerson}>
            {children ? children : null}
        </div>
    )
};

export default Floor;