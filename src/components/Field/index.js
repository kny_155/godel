import React from 'react';
import Floor from "../Floor";

import './style.css'
import Person from "../Person";

const Field = ({field, xPerson, yPerson, direction}) => {



    const copyField = () => {
        const newField = [];
        field.forEach(item => newField.push([...item]));
        return newField
    };

    let newField = copyField();

    newField[yPerson][xPerson] = (
        <Floor key={xPerson + "_" + yPerson}>
            <Person direction={direction}/>
        </Floor>
    );


    return (
        <div className="field">
            {newField}
        </div>
    );
};

export default Field;